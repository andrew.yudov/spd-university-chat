import React from 'react';
import {CURRENT_USER_AVATAR_URL} from '../constants';

import './header.css';


export const Header = () => (
    <header className="header">
        <h1 className="header__title">SPDU</h1>
        <div className="header__avatar">
            <img className="header__image" src={CURRENT_USER_AVATAR_URL} alt="Current User Avatar"/>
        </div>
    </header>
);
