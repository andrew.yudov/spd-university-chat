import React, {useState} from 'react';

import './main.css';
import {Message} from './message/message';
import {ENTER_KEY_CODE, INITIAL_MESSAGES} from './constants';
import {getRandomMessage} from './utils/getRandomMessage';
import {getMessage} from './utils/getMessage';


export const Main = () => {
    const [messages, setMessages] = useState(INITIAL_MESSAGES);
    const [currentMessage, setCurrentMessage] = useState('');

    const onMessageReply = () => {
        setTimeout(() => (
            setMessages(prevState => ([
                ...prevState, getMessage(prevState, getRandomMessage().text, false)
            ]))
        ), 1000);
    };
    const onTextAreaChange = ({target: {value}}) => {
        setCurrentMessage(value);
    };
    const onTextAreaKeyDown = ({keyCode}) => {
        if (keyCode === ENTER_KEY_CODE) {
            onApply();
        }
    }
    const onApply = () => {
        if (currentMessage.trim()) {
            setMessages([...messages, getMessage(messages, currentMessage)]);
            setCurrentMessage('');
            onMessageReply();
        }
    };
    const deleteMessage = id => {
        setMessages(prevState => prevState.filter(message => id !== message.id))
    };

    return (
        <div className="main">
            <div className="main__messages">
                {messages.map(message => (
                    <Message key={message.id} message={message} deleteMessage={deleteMessage}/>
                ))}
            </div>
            <div className="main__action-buttons">
                <div className="main__section">
                    <textarea
                        className="main__textarea"
                        onChange={onTextAreaChange}
                        onKeyDown={onTextAreaKeyDown}
                        value={currentMessage}
                    />
                </div>
                <div className="main__section">
                    <button className="main__button" onClick={onApply}>Send message</button>
                </div>
            </div>
        </div>
    )
};
