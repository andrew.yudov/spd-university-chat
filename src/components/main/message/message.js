import React from 'react';
import closeIcon from '../../../images/close-icon.png';
import {CURRENT_USER_AVATAR_URL} from '../../constants';

import './message.css';

const SECOND_USER_AVATAR_URL = 'https://randomuser.me/api/portraits/lego/2.jpg';

export const Message = ({message: {id, text, isCurrentUser}, deleteMessage}) => {
    const onRemoveIconClick = () => {
        deleteMessage(id);
    }

    return (
        <div className={`message ${isCurrentUser ? '' : 'message_second'}`}>
            <div className="message__avatar">
                <img
                    className="message__image"
                    src={isCurrentUser ? CURRENT_USER_AVATAR_URL : SECOND_USER_AVATAR_URL}
                    alt="lego"
                />
            </div>
            <div className="message__bubble">
                <div className="message__text">{text}</div>
                {isCurrentUser && <div className="message__remove-button">
                    <img src={closeIcon} alt="Close icon" onClick={onRemoveIconClick}/>
                </div>}
            </div>
        </div>
    );
};
